UCL-Spielerliste: 
- Ist eine Graphische Oberfläche um Spieler von einer Mannschaft oder einem Turnier aufzulisten. 
  Es können Spielernamen, Mannschaften, Positionen und Trickotfarben angegeben werden. Diese Spielerdaten werden in einer Tabelle
  ausgeben und in einem 3D Raum gezeichnet. Es ist möglich die Listen mit zwei verschiedenen Datenformate (.txt und .xls) abzuspeichern. 

Benötigte Tools:
- Visual Studio
- Putty
- Microsoft Excell

Spieler einfügen:
- Die Textfelder Name, Position, Mannschaft ausfüllen und Trickotfarbe auswählen. Nachdem man diese Daten angegeben hat auf den Einfügen
  Button klicken. Es sollten nun die angegeben Daten in den Listboxen eingefügt sein. Zusätzlich wird mit Helix eine Spielerfigur gezeichnet

Zufälliger Spielername:
- Fügt einen Zufälligen Namen aus einem Webserver in die Textbox hinein.

Spieler entfernen:
- Wenn man in der Liste einen Spieler entfernen möchte, muss man den Spieler, seine Position oder seine Mannschaft auswählen und auf den
  Entfernen Button klicken, damit er aus der Liste entfernt werden kann.

Daten in der Liste verändern:
- Wenn man Daten in der Liste verbessern/verändern möchte, muss man zuerst zum Beispiel den Spielernamen anklicken. Danach ist in die Textbox,
  wo man den Spielernamen angeben kann, der neue Name anzugeben. Anschließen klickt man auf den Verändern Button und der Name der markiert 
  wurde wird verändert.

Excell Tabelle speichern:
- Durch anklicken des Tabelle Speichern Buttons wird die akutelle Liste in eine Excell Tabelle abgespeichert. Diese Datei befindet sich unter
  Dokumente mit dem Namen UCL-Spielerliste.xls. 

Menüleiste Datei:
- Neu: Leert alle Daten in der Liste
- Öffnen: Kann aus einer .txt Datei eine gespeicherte Liste öffnen
- Speichern: Speichert die aktuelle Liste in ein .txt File ab
- Beenden: schließt das Fenster

Menüleiste Options:
- Hat keine Funktion dient nur zum Visuellen damit die Menüleiste nicht so leer aussieht. =)


BUGS:
- Wenn man das Programm startet und wieder schließen möchte, läuft der Debug noch weiter. Man muss extra, nachdem man das Fenster 
  geschlossen hat, den Debug beenden.
- Manchmal kann es sein wenn man zu oft zufällige Namen anfordert, das der Webserver die Verbindung nicht erlaubt. Daher muss man
  ein Paar Minuten warten bis wieder neue random Namen angefordert werden können.

