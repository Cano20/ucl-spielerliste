﻿using System;
using UCL_Spielerliste;
using System.ComponentModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;

namespace UCLS_UnitTest
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestMethodPlayerObject()    // Can
        {
            string expectedName = "Can";
            string expectedPosition = "Striker";
            string expectedTeam = "Fenerbahce";
            Brush expectedKitCol = Brushes.Blue;

            Player player = new Player(expectedName, expectedTeam, expectedPosition, expectedKitCol);

            Assert.AreEqual(expectedName, player.Name);
            Assert.AreEqual(expectedPosition, player.Position);
            Assert.AreEqual(expectedTeam, player.Team);
            Assert.AreEqual(expectedKitCol, player.DressColor);
        }

        [TestMethod]
        public void TestMethodDrawObject()  // Lukas
        {
            HelixViewport3D expectedcan3D = new HelixViewport3D();
            int expectedAnzahl = 2;

            Player player = new Player();   // neuer Spieler-Objekt
            player.Draw(expectedcan3D, expectedAnzahl);     // Zeichnen mit expected-Parameter

            Assert.IsNotNull(expectedcan3D);        // Abfrage ob gezeichnet wurde
        }

        [TestMethod]
        public void TestMethodSerializeObject()  // Lukas
        {
            Player player = new Player("Lukas", "Bayern", "Midfield", Brushes.Red); // Erzeugen Player-Objekt
            string expectedSerialize = "Lukas:Bayern:Midfield:#FFFF0000";  // expected-Parameter
            Assert.AreEqual(expectedSerialize, player.Serialize()); // Vergleich für Serialize
        }
    }
}