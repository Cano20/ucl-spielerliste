﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace UCL_Spielerliste
{
    public class RestAPI : ObservableCollection<Player>
    {
        // RestAPI wird nur bei den Spielernamen verwendet, da es keinen Generator für die Mannschaft und Position gibt

        public void Playername(int anzahl)
        {
            WebClient webClient = new WebClient();
            webClient.DownloadDataCompleted += WebClient_DownloadDataCompleted;
            webClient.DownloadDataAsync(new Uri($"https://uinames.com/api/?amount={anzahl}"));
        }

        private void WebClient_DownloadDataCompleted(object sender, DownloadDataCompletedEventArgs e)
        {
            Clear();
            byte[] resultraw = e.Result;
            string result = Encoding.UTF8.GetString(resultraw);
            if (result[0] == '[')
            {
                var items = JsonConvert.DeserializeObject<List<Player>>(result);
                foreach (var item in items)
                {
                    Add(item);
                }
            }
            else
            {
                var item = JsonConvert.DeserializeObject<Player>(result);
                Add(item);
            }
        }
    }
}