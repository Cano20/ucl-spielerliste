﻿using HelixToolkit.Wpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EXCEL = Microsoft.Office.Interop.Excel;

namespace UCL_Spielerliste
{
    public partial class MainWindow : Window
    {

        #region Defintionen/Variablen

        EXCEL.Application xlsApp;

        Socket socket;
        Brush brush;
        const int BUFFERSIZE = 256;
        Random random = new Random();

        Player spieler = new Player();
        PlayerList Liste = new PlayerList();
        PlayerList randomname = new PlayerList();

        RestAPI playername = new RestAPI();

        public event EventHandler<DataReceiveEventArgs> DataReceive;

        #endregion

        public MainWindow()
        {
            InitializeComponent();
            DataReceive += MainWindow_DataReceive;
        }

        #region Putty_Communication
        private void MainWindow_DataReceive(object sender, DataReceiveEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                if (e.Message == "\r\n")    
                {
                    return;
                }
                else
                {
                    #region Message_Split
                    Brush color = null;
                    string[] tokens = e.Message.Split(':');
                    string command = tokens[0];
                    string name = tokens[1];
                    string position = tokens[2];
                    string mannschaft = tokens[3];
                    string trikotfarbe = tokens[4];

                    if (trikotfarbe == "Schwarz" || trikotfarbe == "schwarz" || trikotfarbe == "Black" || trikotfarbe == "black")
                        color = Brushes.Black;
                    else if (trikotfarbe == "Weiß" || trikotfarbe == "weiß" || trikotfarbe == "White" || trikotfarbe == "white" || trikotfarbe == "Weis" || trikotfarbe == "weis")
                        color = Brushes.White;
                    else if (trikotfarbe == "Rot" || trikotfarbe == "rot" || trikotfarbe == "Red" || trikotfarbe == "red")
                        color = Brushes.Red;
                    else if (trikotfarbe == "Blau" || trikotfarbe == "blau" || trikotfarbe == "Blue" || trikotfarbe == "blue")
                        color = Brushes.Blue;
                    else if (trikotfarbe == "Grün" || trikotfarbe == "grün" || trikotfarbe == "Green" || trikotfarbe == "green")
                        color = Brushes.Green;
                    else if (trikotfarbe == "Gelb" || trikotfarbe == "gelb" || trikotfarbe == "Yellow" || trikotfarbe == "yellow")
                        color = Brushes.Yellow;
                    #endregion

                    #region Add_Command
                    if (command == "add" || command == "Add" || command == "ADD")
                    {
                        LstBox_Name.Items.Add(name);
                        LstBox_Mannschaft.Items.Add(mannschaft);
                        LstBox_Position.Items.Add(position);

                        spieler = new Player(name, mannschaft, position, color);

                        Liste.Add(spieler);
                        Liste.Draw(can3D);
                    }
                    #endregion

                    #region Remove_Command
                    else if (command == "remove" || command == "Remove" || command == "REMOVE")
                    {
                        List<Player> player = new List<Player>();
                        player = Liste.List();
                        for (int i = 0; i < Liste.Count(); i++)
                        {
                            if (player[i].Name.ToString().Contains(name))
                            {
                                Liste.Remove(player[i]);
                                LstBox_Name.Items.Remove(LstBox_Name.Items[i]);
                                LstBox_Mannschaft.Items.Remove(LstBox_Mannschaft.Items[i]);
                                LstBox_Position.Items.Remove(LstBox_Position.Items[i]);
                            }
                        }

                        can3D.Children.Clear();
                        SunLight sun = new SunLight();
                        sun.ShowLights = true;
                        can3D.Children.Add(sun);
                        Liste.Draw(can3D);
                    }
                    #endregion
                }
            }));
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Thread t = new Thread(() =>
                StartListening(IPAddress.Any, 50000));
            t.Start();
        }

        private void StartListening(IPAddress iPAddress, int port)
        {
            socket = new Socket(iPAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint ipe = new IPEndPoint(iPAddress, port);
            socket.Bind(ipe);
            socket.Listen(5);
            brush = Brushes.White;
            ProcessSocket(socket.Accept(), brush);
        }

        private void ProcessSocket(Socket rcv, Brush brush)
        {
            Thread t = new Thread(() =>
            {
                ProcessSocket(socket.Accept(), Brushes.White);
            });
            t.Start();

            byte[] buffer = new byte[BUFFERSIZE];
            int rxlength = 0;
            string input = "";
            ASCIIEncoding enc = new ASCIIEncoding();

            while ((rxlength = rcv.Receive(buffer)) > 0)
            {
                input = enc.GetString(buffer, 0, rxlength);

                DataReceiveEventArgs eventArgs = new DataReceiveEventArgs
                {
                    Message = input,
                    Brush = brush,
                    Source = rcv.RemoteEndPoint as IPEndPoint
                };

                OnDataReceive(eventArgs);
            }
        }

        private void OnDataReceive(DataReceiveEventArgs eventArgs)
        {
            DataReceive?.Invoke(this, eventArgs);
        }
        #endregion

        #region Button_Konfiguration
        private void btn_hinzufügen(object sender, RoutedEventArgs e)
        {
            #region Trikotfarben
            Brush color = Brushes.Black;
            if (ComBox.Text == "Schwarz")
                color = Brushes.Black;
            if (ComBox.Text == "Weiß")
                color = Brushes.White;
            if (ComBox.Text == "Rot")
                color = Brushes.Red;
            if (ComBox.Text == "Blau")
                color = Brushes.Blue;
            if (ComBox.Text == "Grün")
                color = Brushes.Green;
            if (ComBox.Text == "Gelb")
                color = Brushes.Yellow;
            #endregion


            #region Hinzufügen
            spieler = new Player(txtb_name_of_player.Text, txtb_mannschaft_name.Text, txtb_position_player.Text, color);
            Liste.Add(spieler);
            Liste.Draw(can3D);

            LstBox_Name.Items.Add(txtb_name_of_player.Text);
            LstBox_Mannschaft.Items.Add(txtb_mannschaft_name.Text);
            LstBox_Position.Items.Add(txtb_position_player.Text);
            #endregion


            #region Leeren
            txtb_mannschaft_name.Clear();
            txtb_name_of_player.Clear();
            txtb_position_player.Clear();
            ComBox.SelectedIndex = 0;
            #endregion
        }

        private void btn_randomName(object sender, RoutedEventArgs e)
        {
            string url = "https://uinames.com/api/";
            string json = new WebClient().DownloadString(url);

            string[] tokens = json.Split('{');

            string string1 = tokens[1];
            string[] tokens1 = string1.Split(':');

            string string2 = tokens1[1];
            string[] tokens2 = string2.Split(',');

            string string3 = tokens2[0];
            string[] tokens3 = string3.Split('"');

            string name = tokens3[1];

            txtb_name_of_player.Text = name;
        }

        private void btn_loeschen(object sender, RoutedEventArgs e)
        {
            int index;

            #region Listbox_Name
            if (LstBox_Name.SelectedItem != null)
            {
                index = LstBox_Name.SelectedIndex;

                List<Player> player = new List<Player>();
                player = Liste.List();
                for (int i = 0; i < Liste.Count(); i++)
                {
                    if (player[i].Name.ToString().Contains(LstBox_Name.SelectedItem.ToString()))
                    {
                        Liste.Remove(player[i]);
                    }
                }   

                LstBox_Name.Items.Remove(LstBox_Name.Items[index]);
                LstBox_Mannschaft.Items.Remove(LstBox_Mannschaft.Items[index]);
                LstBox_Position.Items.Remove(LstBox_Position.Items[index]);

            }
            #endregion

            #region Listbox_Mannschaft
            if (LstBox_Mannschaft.SelectedItem != null)
            {
                index = LstBox_Mannschaft.SelectedIndex;

                List<Player> player = new List<Player>();
                player = Liste.List();
                for (int i = 0; i < Liste.Count(); i++)
                {
                    if (player[i].Team.ToString().Contains(LstBox_Mannschaft.SelectedItem.ToString()))
                    {
                        Liste.Remove(player[i]);
                    }
                }

                LstBox_Name.Items.Remove(LstBox_Name.Items[index]);
                LstBox_Mannschaft.Items.Remove(LstBox_Mannschaft.Items[index]);
                LstBox_Position.Items.Remove(LstBox_Position.Items[index]);
            }
            #endregion

            #region Listbox_Postion
            if (LstBox_Position.SelectedItem != null)
            {
                index = LstBox_Position.SelectedIndex;

                List<Player> player = new List<Player>();
                player = Liste.List();
                for (int i = 0; i < Liste.Count(); i++)
                {
                    if (player[i].Position.ToString().Contains(LstBox_Position.SelectedItem.ToString()))
                    {
                        Liste.Remove(player[i]);
                    }
                }

                LstBox_Name.Items.Remove(LstBox_Name.Items[index]);
                LstBox_Mannschaft.Items.Remove(LstBox_Mannschaft.Items[index]);
                LstBox_Position.Items.Remove(LstBox_Position.Items[index]);
            }
            #endregion

            #region Neu_Zeichnen
            can3D.Children.Clear();
            SunLight sun = new SunLight();
            sun.ShowLights = true;
            can3D.Children.Add(sun);
            Liste.Draw(can3D);
            #endregion
        }

        private void btn_veraendern(object sender, RoutedEventArgs e)
        {
            #region Change
            int index;
            List<Player> player = new List<Player>();
            player = Liste.List();

            if (txtb_name_of_player.Text != "")
            {
                index = LstBox_Name.SelectedIndex;

                if (LstBox_Name.SelectedItem != null)
                {
                    for (int i = 0; i < Liste.Count(); i++)
                    {
                        if (player[i].Name.ToString().Contains(LstBox_Name.SelectedItem.ToString()))
                        {
                            player[i].Name = txtb_name_of_player.Text;
                            break;
                        }
                    }
                }
            }

            if (txtb_mannschaft_name.Text != "")
            {
                index = LstBox_Mannschaft.SelectedIndex;

                if (LstBox_Mannschaft.SelectedItem != null)
                {
                    for (int i = 0; i < Liste.Count(); i++)
                    {
                        if (player[i].Team.ToString().Contains(LstBox_Mannschaft.SelectedItem.ToString()))
                        {
                            player[i].Team = txtb_mannschaft_name.Text;
                            break;
                        }
                    }
                }
            }

            if (txtb_position_player.Text != "")
            {
                index = LstBox_Position.SelectedIndex;

                if (LstBox_Position.SelectedItem != null)
                {
                    for (int i = 0; i < Liste.Count(); i++)
                    {
                        if (player[i].Position.ToString().Contains(LstBox_Position.SelectedItem.ToString()))
                        {
                            player[i].Position = txtb_position_player.Text;
                            break;
                        }
                    }
                }
            }
            #endregion

            #region Clear
            LstBox_Mannschaft.Items.Clear();
            LstBox_Name.Items.Clear();
            LstBox_Position.Items.Clear();

            txtb_mannschaft_name.Clear();
            txtb_name_of_player.Clear();
            txtb_position_player.Clear();
            #endregion

            #region NewList
            player = Liste.List();
            for (int i = 0; i < Liste.Count(); i++)
            {
                LstBox_Name.Items.Add(player[i].Name);
                LstBox_Mannschaft.Items.Add(player[i].Team);
                LstBox_Position.Items.Add(player[i].Position);
            }
            #endregion

            #region Neu_Zeichnen
            can3D.Children.Clear();
            SunLight sun = new SunLight();
            sun.ShowLights = true;
            can3D.Children.Add(sun);
            Liste.Draw(can3D);
            #endregion
        }

        private void btn_EXCELSave(object sender, RoutedEventArgs e)
        {
            #region Excel_Save
            xlsApp = new EXCEL.Application();
            EXCEL.Workbook xlsWorkbook = xlsApp.Workbooks.Add();            //Excell File
            EXCEL.Worksheet xlsWorksheet;                                   //Excell Tabellen

            if (xlsWorkbook.Worksheets.Count == 0)
                xlsWorksheet = xlsWorkbook.Worksheets.Add();
            else
                xlsWorksheet = xlsWorkbook.Worksheets.Item[1];              //Indexes sind in COM 1-Basierend

            xlsWorksheet.Cells[2, 2] = "Name";
            xlsWorksheet.Cells[2, 3] = "Position";
            xlsWorksheet.Cells[2, 4] = "Mannschaft";
            xlsWorksheet.Cells[2, 5] = "Trikotfarbe";

            List<Player> list = new List<Player>();

            list = Liste.List();

            for (int i = 0; i < Liste.Count(); i++)
            {
                int n = i + 3;
                xlsWorksheet.Cells[n, 2] = list[i].Name;
                xlsWorksheet.Cells[n, 3] = list[i].Position;
                xlsWorksheet.Cells[n, 4] = list[i].Team;
                xlsWorksheet.Cells[n, 5] = list[i].DressColor.ToString();
            }

            xlsApp.DisplayAlerts = false;                                   //unterdrückt Warnungen (Speichern, Überschreiben, etc)

            xlsWorkbook.SaveAs("UCL_Spielerliste.xls");                     //Wird unter Dokumente abgespeichert
            xlsWorkbook.Save();
            xlsApp.Quit();
            #endregion
        }

        #endregion

        #region Menu_Konfiguration
        private void MenItem_New(object sender, RoutedEventArgs e)
        {
            #region Reset
            List<Player> player = new List<Player>();
            player = Liste.List();
            for (int i = 0; i < Liste.Count(); i++)
            {
                Liste.Remove(player[i]);
            }
            List<Player> test = new List<Player>();
            test = Liste.List();
            for (int i = 0; i < Liste.Count(); i++)
            {
                Liste.Remove(test[i]);
            }

            txtb_mannschaft_name.Clear();
            txtb_name_of_player.Clear();
            txtb_position_player.Clear();
            ComBox.SelectedIndex = 0;

            LstBox_Mannschaft.Items.Clear();
            LstBox_Name.Items.Clear();
            LstBox_Position.Items.Clear();

            can3D.Children.Clear();
            SunLight sun = new SunLight();
            sun.ShowLights = true;
            can3D.Children.Add(sun);

            #endregion
        }

        private void MenItem_Open(object sender, RoutedEventArgs e)
        {
            #region Reset
            txtb_mannschaft_name.Clear();
            txtb_name_of_player.Clear();
            txtb_position_player.Clear();
            ComBox.SelectedIndex = 0;

            LstBox_Mannschaft.Items.Clear();
            LstBox_Name.Items.Clear();
            LstBox_Position.Items.Clear();

            can3D.Children.Clear();
            #endregion

            #region Open
            OpenFileDialog dlg = new OpenFileDialog();
            List<Player> player = new List<Player>();

            if (dlg.ShowDialog() == true)
            {
                Liste = PlayerList.Open(dlg.FileName);
            }
            player = Liste.List();
            for (int i = 0; i < Liste.Count(); i++)
            {
                LstBox_Name.Items.Add(player[i].Name);
                LstBox_Mannschaft.Items.Add(player[i].Team);
                LstBox_Position.Items.Add(player[i].Position);

                player[i].Draw(can3D, i);
            }
            SunLight sun = new SunLight();
            sun.ShowLights = true;
            can3D.Children.Add(sun);
            #endregion
        }

        private void MenItem_Save(object sender, RoutedEventArgs e)
        {
            #region Save
            SaveFileDialog dlg = new SaveFileDialog();

             if (dlg.ShowDialog() == true)
             {
                Liste.Save(dlg.FileName);
             }
            #endregion
        }

        private void MenItem_Exit(object sender, RoutedEventArgs e)
        {
            Close();
        }
        #endregion
    }
}
