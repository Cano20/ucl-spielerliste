﻿using HelixToolkit.Wpf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCL_Spielerliste
{
    public class PlayerList
    {
        private List<Player> players = new List<Player>();

        public void Add(Player item)
        {
            players.Add(item);
        }

        public void Remove(Player item)
        {
            players.Remove(item);
        }

        public int Count()
        {
            return players.Count();
        }

        public void Clear()
        {
            players.Clear();
        }

        public List<Player> List()
        {
            return players;
        }

        public string Serialize()
        {
            string result = "";
            for (int i = 0; i < players.Count; i++)
            {
                result = result + "%" + players[i].Serialize().ToString();
            }
            return result;
        }

        public PlayerList Deserialize(string Data)
        {
            players.Clear();
            string[] token = Data.Split('%');
            Player spieler = new Player();
            PlayerList neu = new PlayerList();
            for (int i = 0; i < token.Length; i++)
            {
                spieler.Deserialize(token[i]);
                neu.Add(spieler);
            }
            return neu;
        }

        public void Save(string filename)
        {
            StreamWriter save = new StreamWriter(filename);

            foreach (Player item in players)
            {
                save.WriteLine(item.Serialize());
            }
            save.Close();
        }

        public static PlayerList Open(string filename)
        {
            StreamReader open = new StreamReader(filename);
            PlayerList playersnew = new PlayerList();
            Player player = new Player();
            while (!open.EndOfStream)
            {
                string data = open.ReadLine();
                player = player.Deserialize(data);
                playersnew.Add(player);
            }
            open.Close();

            return playersnew;
        }

        public override string ToString()
        {
            string result = "";
            foreach (Player item in players)
            {
                result = result + item.ToString();
            }
            return result;
        }

        public void Draw(HelixViewport3D dev)
        {
            for (int i = 0; i < players.Count; i++)
            {
                players[i].Draw(dev, i);
            }
        }
    }
}
