﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace UCL_Spielerliste
{
    public class DataReceiveEventArgs : EventArgs
    {
        public IPEndPoint Source { get; set; }

        public string Message { get; set; }

        public Brush Brush { get; set; }
    }
}
