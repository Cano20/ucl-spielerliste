﻿using HelixToolkit.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace UCL_Spielerliste
{
    public class Player
    {
        #region Mitgliedsvariablen
        private string name;
        private string team;
        private string position;
        private Brush dressColor;
        #endregion

        #region Properties
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Team  
        {
            get { return team; }
            set { team = value; }
        }

        public string Position
        {
            get { return position; }
            set { position = value; }
        }

        public Brush DressColor
        {
            get { return dressColor; }
            set { dressColor = value; }
        }
        #endregion

        public Player(string Name, string Team, string Position, Brush Color)
        {
            name = Name;
            team = Team;
            position = Position;
            dressColor = Color;
        }

        public Player()
        {

        }

        public string Serialize()
        {
            string result;
            result = $"{Name}:{Team}:{Position}:{DressColor}";
            return result;
        }

        public Player Deserialize(string data)
        {
            Brush color = null;

            string[] tokens = data.Split(':');
            string name = tokens[0];
            string team = tokens[1];
            string position = tokens[2];
            string dresscolor = tokens[3];

            if (dresscolor == "#FF000000")
                color = Brushes.Black;
            else if (dresscolor == "#FFFFFFFF")
                color = Brushes.White;
            else if (dresscolor == "#FFFF0000")
                color = Brushes.Red;
            else if (dresscolor == "#FF0000FF")
                color = Brushes.Blue;
            else if (dresscolor == "#FF008000")
                color = Brushes.Green;
            else if (dresscolor == "#FFFFFF00")
                color = Brushes.Yellow;


            Player player = new Player(name, team, position, color);

            return player;
        }

        public override string ToString()
        {
            return $"{name}, {team}, {position}, {dressColor}";
        }

        public void Draw(HelixViewport3D can3D, int Anzahl)
        {
            int abstand = Anzahl * -7;
            Brush br = dressColor;

            #region Name
            TextVisual3D name_of_player = new TextVisual3D();
            name_of_player.Text = name;
            name_of_player.Height = 2;
            name_of_player.FontWeight = FontWeights.Normal;
            name_of_player.Position = new Point3D(0 + abstand, 0, 5);

            can3D.Children.Add(name_of_player);
            #endregion

            #region Kopf
            SphereVisual3D kopf = new SphereVisual3D();
            kopf.Center = new Point3D(0 + abstand, 0, 2);
            kopf.Fill = Brushes.LightPink; 
            kopf.Radius = 1;

            can3D.Children.Add(kopf);
            #endregion

            #region Arm
            PipeVisual3D armR = new PipeVisual3D();
            armR.Fill = Brushes.LightPink;
            armR.Point1 = new Point3D(0.5 + abstand, 0, 0.5);
            armR.Point2 = new Point3D(2.5 + abstand, 0, -0.2);

            can3D.Children.Add(armR);

            PipeVisual3D armL = new PipeVisual3D();
            armL.Fill = Brushes.LightPink;
            armL.Point1 = new Point3D(-0.5 + abstand, 0, 0.5);
            armL.Point2 = new Point3D(-2.5 + abstand, 0, -0.2);

            can3D.Children.Add(armL);
            #endregion

            #region Körper
            CubeVisual3D koerper = new CubeVisual3D();
            koerper.Center = new Point3D(0 + abstand, 0, 0);
            koerper.Fill = br;
            koerper.SideLength = 2;

            can3D.Children.Add(koerper);
            #endregion

            #region Schenkel
            CubeVisual3D schenkelR = new CubeVisual3D();
            schenkelR.Center = new Point3D(0.7 + abstand, 0, -1.2);
            schenkelR.Fill = br;
            schenkelR.SideLength = 1.1;

            can3D.Children.Add(schenkelR);

            CubeVisual3D schenkelL = new CubeVisual3D();
            schenkelL.Center = new Point3D(-0.7 + abstand, 0, -1.2);
            schenkelL.Fill = br;
            schenkelL.SideLength = 1.1;

            can3D.Children.Add(schenkelL);
            #endregion

            #region Fuß
            PipeVisual3D fußR = new PipeVisual3D();
            fußR.Fill = Brushes.LightPink;
            fußR.Point1 = new Point3D(0.7 + abstand, 0, -1.2);
            fußR.Point2 = new Point3D(0.7 + abstand, 0, -2.7);

            can3D.Children.Add(fußR);

            PipeVisual3D fußL = new PipeVisual3D();
            fußL.Fill = Brushes.LightPink;
            fußL.Point1 = new Point3D(-0.7 + abstand, 0, -1.2);
            fußL.Point2 = new Point3D(-0.7 + abstand, 0, -2.7);

            can3D.Children.Add(fußL);
            #endregion
        }
    }
}
